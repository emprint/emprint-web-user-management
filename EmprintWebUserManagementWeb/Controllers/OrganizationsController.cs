using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmprintWebUserManagement.Implementations.Authorizers;
using EmprintWebUserManagement.Implementations.FacilityRepositories;
using EmprintWebUserManagement.Implementations.OrganizationRepositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EmprintWebUserManagement.Web.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    public class OrganizationsController : Controller
    {
        private readonly Core _core;
        private readonly AzureTableAuthorizer _authorizer;
        private readonly AzureDatabaseOrganizationRepository _organizationsRepository;

        public OrganizationsController(IConfiguration configuration)
        {
            _core = new Core();
            _authorizer = new AzureTableAuthorizer(configuration.GetConnectionString("AzureTableAuthorizer"),
                configuration.GetSection("Data")["AzureTableAuthorizerTableName"]);
            _organizationsRepository = new AzureDatabaseOrganizationRepository();
        }

        [HttpGet]
        public JsonResult Get()
        {
            var coreResponse = _core.HandleEmprintWebOrganizationRequest(
                Utilities.GetEmprintWebOrganizationsRequest(Request),
                _authorizer, _organizationsRepository);
            Response.StatusCode = Utilities.GetHttpStatusCode(coreResponse, Request);
            return Json(Utilities.GetHttpBody(coreResponse, Request));
        }
    }
}