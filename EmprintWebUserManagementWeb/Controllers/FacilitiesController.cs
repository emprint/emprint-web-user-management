using EmprintWebUserManagement.Implementations.Authorizers;
using EmprintWebUserManagement.Implementations.FacilityRepositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EmprintWebUserManagement.Web.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    public class FacilitiesController : Controller
    {
        private readonly Core _core;
        private readonly AzureTableAuthorizer _authorizer;
        private readonly AzureDatabaseFacilityRepository _facilitiesRepository;

        public FacilitiesController(IConfiguration configuration)
        {
            _core = new Core();
            _authorizer = new AzureTableAuthorizer(configuration.GetConnectionString("AzureTableAuthorizer"),
                configuration.GetSection("Data")["AzureTableAuthorizerTableName"]);
            _facilitiesRepository = new AzureDatabaseFacilityRepository();
        }

        [HttpGet]
        public JsonResult Get()
        {
            var coreResponse = _core.HandleEmprintWebFacilitiesRequest(
                Utilities.GetEmprintWebFacilitiesRequest(Request),
                _authorizer, _facilitiesRepository);
            Response.StatusCode = Utilities.GetHttpStatusCode(coreResponse, Request);
            return Json(Utilities.GetHttpBody(coreResponse, Request));
        }
    }
}