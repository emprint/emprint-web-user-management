using System.Net;
using EmprintWebUserManagement.Implementations.Authorizers;
using Microsoft.AspNetCore.Mvc;
using EmprintWebUserManagement.Web.Models;
using EmprintWebUserManagement.Implementations.UserRepositories;
using EmprintWebUserManagement.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace EmprintWebUserManagement.Web.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    public class UsersController : Controller
    {
        private readonly Core _core;
        private readonly AzureTableAuthorizer _authorizer;
        private readonly AzureTableUserRepository _userRepository;

        public UsersController(IConfiguration configuration)
        {
            _core = new Core();
            _authorizer = new AzureTableAuthorizer(configuration.GetConnectionString("AzureTableAuthorizer"),
                configuration.GetSection("Data")["AzureTableAuthorizerTableName"]);
            _userRepository = new AzureTableUserRepository(configuration.GetConnectionString("AzureTableUserRepository"),
                configuration.GetSection("Data")["AzureTableUserPermissionsRepositoryTableName"]);
        }

        [HttpDelete("{userName}")]
        public JsonResult Delete(string userName)
        {
            var coreResponse = _core.HandleEmprintWebUserRequest(
                    Utilities.GetEmprintWebUserRequest(userName, Request),
                    _authorizer, _userRepository);
            Response.StatusCode = Utilities.GetHttpStatusCode(coreResponse, Request);
            return Json(Utilities.GetHttpBody(coreResponse, Request));
        }

        [HttpGet("{userName}")]
        public JsonResult Get(string userName)
        {
            var coreResponse = _core.HandleEmprintWebUserRequest(
                Utilities.GetEmprintWebUserRequest(userName, Request),
                _authorizer, _userRepository);
            Response.StatusCode = Utilities.GetHttpStatusCode(coreResponse, Request);
            return Json(Utilities.GetHttpBody(coreResponse, Request));

        }

        [HttpPost]
        public JsonResult Post([FromBody]UserModel user)
        {
            // Verify the UserModel
            if (Utilities.IsUserModelWellDefined(user))
            {

                var coreResponse = _core.HandleEmprintWebUserRequest(
                    Utilities.GetEmprintWebUserRequest(user, Request),
                    _authorizer, _userRepository);
                Response.StatusCode = Utilities.GetHttpStatusCode(coreResponse, Request);
                Response.Headers["Location"] = Utilities.GetResourceLocation(coreResponse, Request);
                return Json(Utilities.GetHttpBody(coreResponse, Request));
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(Utilities.GenerateUserModelError());

        }

        [HttpPut("{userName}")]
        public JsonResult Put([FromBody] UserModel user)
        {
            var coreResponse = _core.HandleEmprintWebUserRequest(
                Utilities.GetEmprintWebUserRequest(user, Request),
                _authorizer, _userRepository);
            Response.StatusCode = Utilities.GetHttpStatusCode(coreResponse, Request);
            return Json(Utilities.GetHttpBody(coreResponse, Request));
        }
    }
}