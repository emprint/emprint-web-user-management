﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Utility;
using EmprintWebUserManagement.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace EmprintWebUserManagement.Web
{
    public static class Utilities
    {
        private static readonly string AuthorizationHeaderName = "Authorization";

        public static bool IsUserModelWellDefined(UserModel user)
        {
            // Check for required username
            if (user?.Username == null)
            {
                return false;
            }

            // If facility permissions are passed, ensure they have all necessary criteria
            if (user.FacilityPermissions != null)
            {
                foreach (var facilityPermission in user.FacilityPermissions)
                {
                    if (facilityPermission.OrganizationId == null || facilityPermission.FacilityId == null)
                    {
                        return false;
                    }
                }
            }

            // If organization permissions are passed, ensure they have all necessary criteria
            if (user.OrganizationPermissions != null)
            {
                if (user.OrganizationPermissions.OrganizationId == null)
                {
                    return false;
                }
            }

            return true;
        }

        public static EmprintWebOrganizationsRequest GetEmprintWebOrganizationsRequest(HttpRequest request)
        {
            return new EmprintWebOrganizationsRequest
            {
                RequesterInformation = new EmprintWebOrganizationsRequesterInformation
                {
                    SecurityToken = GetAuthorizationToken(request)
                },
                Operation = GetHttpOperation(request)
            };
        }

        public static EmprintWebFacilitiesRequest GetEmprintWebFacilitiesRequest(HttpRequest request)
        {
            return new EmprintWebFacilitiesRequest
            {
                RequesterInformation = new EmprintWebFacilitiesRequesterInformation
                {
                    SecurityToken = GetAuthorizationToken(request), 
                },
                Operation = GetHttpOperation(request)
            };
        }

        public static EmprintWebUserRequest GetEmprintWebUserRequest(UserModel user, HttpRequest request)
        {

            return new EmprintWebUserRequest
            {
                RequesterInformation = new EmprintWebUserRequesterInformation
                {
                    SecurityToken = GetAuthorizationToken(request)
                },
                FacilityPermissions = GetPermissions(user?.FacilityPermissions),
                OrganizationPermissions = GetPermissions(user?.OrganizationPermissions),
                Operation = GetHttpOperation(request),
                Username = user?.Username
            };
        }

        public static EmprintWebUserRequest GetEmprintWebUserRequest(
            string userName, HttpRequest request)
        {
            return new EmprintWebUserRequest
            {
                RequesterInformation = new EmprintWebUserRequesterInformation
                {
                    SecurityToken  = GetAuthorizationToken(request)
                },
                Username = userName,
                Operation = GetHttpOperation(request)
            };
        }

        private static EmprintWebUserOrganizationPermissions GetPermissions(OrganizationPermissionsModel permissions)
        {
            var result = new EmprintWebUserOrganizationPermissions();
            if (permissions != null)

                result =  new EmprintWebUserOrganizationPermissions
                {
                    ApplyPhi = permissions.ApplyPhi,
                    OrganizationId = permissions.OrganizationId
                };
            return result;
        }
        private static List<EmprintWebUserFacilityPermissions> GetPermissions(List<FacilityPermissionsModel> permissions)
        {
            var result = new List<EmprintWebUserFacilityPermissions>();
            if (permissions != null)
            {
                result = permissions.Select(permission => new EmprintWebUserFacilityPermissions
                    {
                        ApplyPhi = permission.ApplyPhi,
                        Facility = permission.FacilityId,
                        OrganizationId = permission.OrganizationId
                    })
                    .ToList();
            }
            return result;

        }

        private static Operation GetHttpOperation(HttpRequest request)
        {
            Operation operation;
            switch (request.Method)
            {
                case "GET":
                    operation = Operation.Read;
                    break;
                case "POST":
                    operation = Operation.Create;
                    break;
                case "PUT":
                    operation = Operation.Update;
                    break;
                case "DELETE":
                    operation = Operation.Delete;
                    break;
                default:
                    operation = Operation.Unknown;
                    break;
            }
            return operation;
        }

        private static string GetAuthorizationToken(HttpRequest request)
        {
            if (IsValidAuthorizationHeader(request))
            {
                return GetAuthorizationHeader(request).Split(' ')[1].TrimEnd(':');
            }
            return null;
        }

        private static bool IsValidAuthorizationHeader(HttpRequest request)
        {
            return AuthorizationHeaderExists(request)
                && AuthorizationHeaderIsProperlyFormated(GetAuthorizationHeader(request));
        }

        private static string GetAuthorizationHeader(HttpRequest request)
        {
            return request.Headers[AuthorizationHeaderName];
        }

        private static bool AuthorizationHeaderIsProperlyFormated(string authorizationHeader)
        {
            // Ensure Auth Token is of form: "Basic XXXXXXXXX:"
            var authorizationHeaderParts = authorizationHeader.Split(' ');
            return authorizationHeaderParts.Length == 2
                && authorizationHeaderParts.First().Equals("Basic")
                && authorizationHeaderParts.Last().EndsWith(":");
        }

        private static bool AuthorizationHeaderExists(HttpRequest request)
        {
            return !String.IsNullOrEmpty(GetAuthorizationHeader(request));
        }

        public static int GetHttpStatusCode(EmprintWebOrganizationsRequestResponse response, HttpRequest request)
        {
            return GetHttpStatusCode(response.Error, request);
        }

        public static int GetHttpStatusCode(EmprintWebFacilitiesRequestResponse response, HttpRequest request)
        {
            return GetHttpStatusCode(response.Error, request);
        }
        public static int GetHttpStatusCode(EmprintWebUserRequestResponse response, HttpRequest request)
        {
            return GetHttpStatusCode(response.Error, request);

        }

        private static int GetHttpStatusCode(ErrorCode error, HttpRequest request)
        {
            int statusCode;
            switch (error)
            {
                case ErrorCode.Unauthorized:
                    statusCode = (int)HttpStatusCode.Unauthorized;
                    break;
                case ErrorCode.NotFound:
                    statusCode = (int)HttpStatusCode.NotFound;
                    break;
                default:
                    switch (GetHttpOperation(request))
                    {
                        case Operation.Create:
                            statusCode = (int)HttpStatusCode.Created;
                            break;
                        default:
                            statusCode = (int)HttpStatusCode.OK;
                            break;
                    }
                    break;

            }
            return statusCode;
        }



        public static UserModel
            GetUserModel(
                EmprintWebUserRequestResponse response, string userName)
        {
            return new UserModel
            {
                Username = userName,
                OrganizationPermissions = GetPermissions(response.OrganizationPermissions),
                FacilityPermissions = GetPermissions(response.FacilityPermissions)
            };
        }

        private static OrganizationPermissionsModel GetPermissions(EmprintWebUserOrganizationPermissions permissions)
        {
            return new OrganizationPermissionsModel
            {
                ApplyPhi = permissions.ApplyPhi,
                OrganizationId = permissions.OrganizationId
            };
        }

        private static List<FacilityPermissionsModel> GetPermissions(
            List<EmprintWebUserFacilityPermissions> permissions)
        {
            return permissions.Select(permission => new FacilityPermissionsModel
                {
                    ApplyPhi = permission.ApplyPhi,
                    FacilityId = permission.Facility,
                    OrganizationId = permission.OrganizationId
                })
                .ToList();
        }

        public static StringValues GetResourceLocation(EmprintWebUserRequestResponse response, HttpRequest request)
        {
            if (ResourceHasBeenCreated(response, request))
            {
                return $"{request.Scheme}://{request.Host}/{response.Username}";
            }
            return "";
        }

        private static bool ResourceHasBeenCreated(EmprintWebUserRequestResponse response, HttpRequest request)
        {
            return GetHttpStatusCode(response, request).Equals((int)HttpStatusCode.Created);
        }


        public static ErrorModel GenerateUserModelError()
        {
            return new ErrorModel
            {
                Type = "invalid_request_error",
                Message = "Malformed body sent in request."
            };
        }

        private static ErrorModel GenerateError(EmprintWebUserRequestResponse response)
        {
            if (!response.Error.Equals(ErrorCode.None))
            {
                return new ErrorModel
                {
                    Type = GetErrorType(response.Error),
                    Message = response.ErrorMessage
                };
            }
            return null;
        }

        public static ErrorModel GenerateError(EmprintWebFacilitiesRequestResponse response)
        {
            if (!response.Error.Equals(ErrorCode.None))
            {
                return new ErrorModel
                {
                    Type = GetErrorType(response.Error),
                    Message = response.ErrorMessage
                };
            }
            return null;
        }
        public static ErrorModel GenerateError(EmprintWebOrganizationsRequestResponse response)
        {
            if (!response.Error.Equals(ErrorCode.None))
            {
                return new ErrorModel
                {
                    Type = GetErrorType(response.Error),
                    Message = response.ErrorMessage
                };
            }
            return null;
        }


        public static object GetHttpBody(EmprintWebOrganizationsRequestResponse response, HttpRequest request)
        {
            if (!response.Error.Equals(ErrorCode.None))
            {
                return GenerateError(response);
            }
            switch (GetHttpOperation(request))
            {
                case Operation.Read:
                    return GetOrganizationModel(response);
                default:
                    return "";
            }
        }

        public static List<OrganizationModel> GetOrganizationModel(EmprintWebOrganizationsRequestResponse response)
        {
            var result = new List<OrganizationModel>();
            foreach (var organization in response.Organizations)
            {
                result.Add(
                    new OrganizationModel
                    {
                        Id = organization.Id,
                        Name = organization.Name
                    });
            }
            return result;
        }

        public static object GetHttpBody(EmprintWebFacilitiesRequestResponse response, HttpRequest request)
        {
            if (!response.Error.Equals(ErrorCode.None))
            {
                return GenerateError(response);
            }
            switch (GetHttpOperation(request))
            {
                case Operation.Read:
                    return GetFacilityModel(response);
                default:
                    return "";
            }
        }

        public static List<FacilityModel> GetFacilityModel(EmprintWebFacilitiesRequestResponse response)
        {
            var result = new List<FacilityModel>();
            foreach (var facility in response.Facilities)
            {
                result.Add(
                    new FacilityModel
                    {
                        Id = facility.Id,
                        Name = facility.Name,
                        OrganizationId = facility.OrganizationId
                    });
            }
            return result;
        }

        public static object GetHttpBody(EmprintWebUserRequestResponse response, HttpRequest request)
        {
            if (!response.Error.Equals(ErrorCode.None))
            {
                return GenerateError(response);
            }
            switch (GetHttpOperation(request))
            {
                case Operation.Read:
                    return GetUserModel(response, response.Username);
                default:
                    return "";
            }
        }

        private static string GetErrorType(ErrorCode error)
        {
            string errorType = "";
            switch (error)
            {
                case ErrorCode.Unauthorized:
                    errorType = "authentication_error";
                    break;
                case ErrorCode.NotFound:
                    errorType = "invalid_request_error";
                    break;
                default:
                    errorType = "api_error";
                    break;
            }
            return errorType;
        }
    }

}
