﻿using System.Collections.Generic;

namespace EmprintWebUserManagement.Web.Models
{
    public class UserModel
    {
        private string _username;
        public string Username
        {
            get => _username;
            set => _username = value.ToLowerInvariant();
        }
        public List<FacilityPermissionsModel> FacilityPermissions { get; set; }
        public OrganizationPermissionsModel OrganizationPermissions { get; set; }
    }
}
