﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmprintWebUserManagement.Web.Models
{
    public class OrganizationPermissionsModel
    {
        public bool ApplyPhi { get; set; }
        public string OrganizationId { get; set; }
    }
}
