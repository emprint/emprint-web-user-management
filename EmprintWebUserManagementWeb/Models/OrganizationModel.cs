﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmprintWebUserManagement.Web.Models
{
    public class OrganizationModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
