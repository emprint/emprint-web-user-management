﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmprintWebUserManagement.Web.Models
{
    public class ErrorModel
    {
        public string Type { get; set; }
        public string Message { get; set; }
    }
}
