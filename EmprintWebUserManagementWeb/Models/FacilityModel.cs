﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmprintWebUserManagement.Web.Models
{
    public class FacilityModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string OrganizationId { get; set; }
    }
}
