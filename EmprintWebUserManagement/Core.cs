﻿using System.Collections.Generic;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Implementations.UserRepositoryResponses;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement
{
    public class Core
    {
        public EmprintWebOrganizationsRequestResponse HandleEmprintWebOrganizationRequest(
            EmprintWebOrganizationsRequest request, IAuthorizer authorizer,
            IOrganizationRepository organizationRepository)
        {
            IOrganizationRepositoryResponse repositoryResponse = null;

            var authorizationResponse = authorizer.Authorize(request);
            if (authorizationResponse.IsAuthorized)
            {
                repositoryResponse =
                    organizationRepository.HandleOrganizationsRequest(request, authorizationResponse);
            }
            return GetEmprintWebOrganizationRequestResponse(authorizationResponse, repositoryResponse);
        }
        public EmprintWebFacilitiesRequestResponse HandleEmprintWebFacilitiesRequest(EmprintWebFacilitiesRequest request,
            IAuthorizer authorizer, IFacilityRepository facilityRepository)
        {
            IFacilityRepositoryResponse repositoryResponse = null;

            var authorizationResponse = authorizer.Authorize(request);
            if (authorizationResponse.IsAuthorized)
            {
                repositoryResponse = facilityRepository.HandleFacilitiesRequest(request, authorizationResponse);
            }
            return GetEmprintWebFacilitiesRequestResponse(authorizationResponse, repositoryResponse);
        }

        public EmprintWebUserRequestResponse HandleEmprintWebUserRequest(EmprintWebUserRequest request, IAuthorizer authorizer,
            IUserRepository userRepository)
        {
            IUserRepositoryResponse repositoryResponse = null;

            var authorizationResponse = authorizer.Authorize(request);
            if (authorizationResponse.IsAuthorized)
            {
                repositoryResponse = userRepository.HandleUserRequest(request, authorizationResponse);
            }
            return GetEmprintWebUserRequestResponse(authorizationResponse, repositoryResponse);
        }

        private EmprintWebOrganizationsRequestResponse GetEmprintWebOrganizationRequestResponse(
            IAuthorizerResponse authorizerResponse, IOrganizationRepositoryResponse repositoryResponse = null)
        {
            return new EmprintWebOrganizationsRequestResponse
            {
                Error = GetErrorCode(authorizerResponse, repositoryResponse),
                IsAuthorized = authorizerResponse?.IsAuthorized ?? false,
                ErrorMessage = GetErrorMessage(authorizerResponse, repositoryResponse),
                Organizations = repositoryResponse?.Organizations
            };
        }
        private EmprintWebFacilitiesRequestResponse GetEmprintWebFacilitiesRequestResponse(IAuthorizerResponse authorizerResponse,
            IFacilityRepositoryResponse repositoryResponse = null)
        {
            return new EmprintWebFacilitiesRequestResponse
            {
                Error = GetErrorCode(authorizerResponse, repositoryResponse),
                IsAuthorized = authorizerResponse?.IsAuthorized ?? false,
                ErrorMessage = GetErrorMessage(authorizerResponse, repositoryResponse),
                Facilities = repositoryResponse?.Facilities

            };
        }

        private EmprintWebUserRequestResponse GetEmprintWebUserRequestResponse(IAuthorizerResponse authorizerResponse,
            IUserRepositoryResponse repositoryResponse = null)
        {
            return new EmprintWebUserRequestResponse
            {
                Error = GetErrorCode(authorizerResponse, repositoryResponse),
                IsAuthorized = authorizerResponse?.IsAuthorized ?? false,
                FacilityPermissions = repositoryResponse?.FacilityPermissions,
                OrganizationPermissions = repositoryResponse?.OrganizationPermissions,
                ErrorMessage = GetErrorMessage(authorizerResponse, repositoryResponse),
                Username = repositoryResponse?.Username
            };
        }
        private string GetErrorMessage(IAuthorizerResponse authorizerResponse,
            IOrganizationRepositoryResponse repositoryResponse = null)
        {
            if (!authorizerResponse.IsAuthorized)
                return "Unable to properly authenticate.";
            return repositoryResponse?.ErrorMessage;
        }
        private string GetErrorMessage(IAuthorizerResponse authorizerResponse,
            IFacilityRepositoryResponse repositoryResponse = null)
        {
            if (!authorizerResponse.IsAuthorized)
                return "Unable to properly authenticate.";
            return repositoryResponse?.ErrorMessage;
        }
        private string GetErrorMessage(IAuthorizerResponse authorizerResponse,
            IUserRepositoryResponse repositoryResponse = null)
        {
            if (!authorizerResponse.IsAuthorized)
                return "Unable to properly authenticate.";
            return repositoryResponse?.ErrorMessage;
        }
        private ErrorCode GetErrorCode(IAuthorizerResponse authorizerResponse,
            IOrganizationRepositoryResponse repositoryResponse = null)
        {
            if (!authorizerResponse.IsAuthorized)
                return ErrorCode.Unauthorized;
            if (repositoryResponse != null)
                return repositoryResponse.Error;
            return ErrorCode.None;
        }
        private ErrorCode GetErrorCode(IAuthorizerResponse authorizerResponse,
            IFacilityRepositoryResponse repositoryResponse = null)
        {
            if (!authorizerResponse.IsAuthorized)
                return ErrorCode.Unauthorized;
            if (repositoryResponse != null)
                return repositoryResponse.Error;
            return ErrorCode.None;
        }
        private ErrorCode GetErrorCode(IAuthorizerResponse authorizerResponse,
            IUserRepositoryResponse repositoryResponse = null)
        {
            if (!authorizerResponse.IsAuthorized)
                return ErrorCode.Unauthorized;
            if (repositoryResponse != null)
                return repositoryResponse.Error;
            return ErrorCode.None;
        }
    }
}
