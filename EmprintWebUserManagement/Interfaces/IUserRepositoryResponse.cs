﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IUserRepositoryResponse
    {
        ErrorCode Error { get; set; }
        List<EmprintWebUserFacilityPermissions> FacilityPermissions { get; set; }
        EmprintWebUserOrganizationPermissions OrganizationPermissions { get; set; }
        string Username { get; set; }
        string ErrorMessage { get; set; }
    }
}
