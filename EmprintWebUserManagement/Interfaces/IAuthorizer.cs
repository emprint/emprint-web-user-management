﻿using EmprintWebUserManagement.DTO;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IAuthorizer
    {
        IAuthorizerResponse Authorize(EmprintWebUserRequest request);
        IAuthorizerResponse Authorize(EmprintWebFacilitiesRequest request);
        IAuthorizerResponse Authorize(EmprintWebOrganizationsRequest request);
    }
}
