﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IOrganizationRepository
    {
        IOrganizationRepositoryResponse HandleOrganizationsRequest(EmprintWebOrganizationsRequest facilitiesRequest, IAuthorizerResponse authorizerResponse);

    }
}
