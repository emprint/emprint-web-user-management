﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IFacilityRepository
    {
        IFacilityRepositoryResponse HandleFacilitiesRequest(EmprintWebFacilitiesRequest facilitiesRequest, IAuthorizerResponse authorizerResponse);

    }
}
