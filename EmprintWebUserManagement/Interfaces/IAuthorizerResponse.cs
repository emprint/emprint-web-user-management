﻿namespace EmprintWebUserManagement.Interfaces
{
    public interface IAuthorizerResponse
    {
        bool IsAuthorized { get; set; }
    }
}
