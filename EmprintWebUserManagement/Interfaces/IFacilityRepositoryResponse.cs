﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IFacilityRepositoryResponse
    {
        ErrorCode Error { get; set; }
        List<EmprintWebFacility> Facilities { get; set; }
        string ErrorMessage { get; set; }
    }
}
