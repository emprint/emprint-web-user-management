﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IUserRepository
    {
        IUserRepositoryResponse HandleUserRequest(EmprintWebUserRequest userRequest, IAuthorizerResponse authorizerResponse);
    }
}
