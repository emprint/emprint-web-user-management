﻿using System.Collections.Generic;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Interfaces
{
    public interface IOrganizationRepositoryResponse
    {
        ErrorCode Error { get; set; }
        List<EmprintWebOrganization> Organizations { get; set; }
        string ErrorMessage { get; set; }
    }
}
