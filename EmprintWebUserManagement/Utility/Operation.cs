﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmprintWebUserManagement.Utility
{
    public enum Operation : byte
    {
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4,
        Unknown = 5
    }
}
