﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace EmprintWebUserManagement.Utility
{
    public enum ErrorCode : byte
    {
        None = 0,
        Unauthorized = 1,
        Unknown = 2,
        NotFound = 3
    }
}
