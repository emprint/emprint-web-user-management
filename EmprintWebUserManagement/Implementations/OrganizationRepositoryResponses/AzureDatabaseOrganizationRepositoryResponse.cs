﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Implementations.OrganizationRepositoryResponses
{
    public class AzureDatabaseOrganizationRepositoryResponse : IOrganizationRepositoryResponse
    {
        public ErrorCode Error { get; set; }
        public List<EmprintWebOrganization> Organizations { get; set; }
        public string ErrorMessage { get; set; }
    }
}
