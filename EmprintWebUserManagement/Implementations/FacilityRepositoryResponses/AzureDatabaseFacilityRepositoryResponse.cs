﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Implementations.FacilityRepositoryResponses
{
    public class AzureDatabaseFacilityRepositoryResponse : IFacilityRepositoryResponse
    {
        public ErrorCode Error { get; set; }
        public List<EmprintWebFacility> Facilities { get; set; }
        public string ErrorMessage { get; set; }
    }
}
