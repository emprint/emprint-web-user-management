﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Implementations.AuthorizerResponses;
using EmprintWebUserManagement.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace EmprintWebUserManagement.Implementations.Authorizers
{
    public class AzureTableAuthorizer : IAuthorizer
    {
        private readonly CloudTable _authorizationKeyTable;
        private string _domain;

        public AzureTableAuthorizer(string connectionString, string tableName)
        {
            _authorizationKeyTable = CloudStorageAccount.Parse(connectionString)
                .CreateCloudTableClient()
                .GetTableReference(tableName);
        }

        public IAuthorizerResponse Authorize(EmprintWebUserRequest request)
        {
            return new AzureTableAuthorizerResponse
            {
                IsAuthorized = IsAuthorized(request),
                Domain = _domain
            };
        }

        public IAuthorizerResponse Authorize(EmprintWebFacilitiesRequest request)
        {
            return new AzureTableAuthorizerResponse
            {
                IsAuthorized = IsAuthorized(request),
                Domain = _domain
            };
        }
        public IAuthorizerResponse Authorize(EmprintWebOrganizationsRequest request)
        {
            return new AzureTableAuthorizerResponse
            {
                IsAuthorized = IsAuthorized(request),
                Domain = _domain
            };
        }

        private bool IsAuthorized(EmprintWebOrganizationsRequest request)
        {
            return IsProperlyFormattedSecurityToken(request)
                   && KeyIsValid(request);
        }

        private bool IsAuthorized(EmprintWebUserRequest request)
        {
            return IsProperlyFormattedSecurityToken(request)
                   && KeyIsValid(request);
        }


        private bool IsAuthorized(EmprintWebFacilitiesRequest request)
        {
            return IsProperlyFormattedSecurityToken(request)
                   && KeyIsValid(request);
        }

        private bool KeyIsValid(EmprintWebFacilitiesRequest request)
        {
            return KeyIsInAzureTable(GetAzureTableAuthorizationQueryResult(request));
        }

        private bool KeyIsValid(EmprintWebOrganizationsRequest request)
        {
            return KeyIsInAzureTable(GetAzureTableAuthorizationQueryResult(request));
        }

        private bool KeyIsValid(EmprintWebUserRequest request)
        {
            return KeyIsInAzureTable(GetAzureTableAuthorizationQueryResult(request));
        }

        private bool KeyIsInAzureTable(List<AzureAuthorizationKey> resultRows)
        {
            if (AzureResultRowsAreProperlyFormatted(resultRows))
            {
                _domain = GetDomainFromResultRows(resultRows);
                return true;
            }
            return false;
        }

        private string GetDomainFromResultRows(List<AzureAuthorizationKey> resultRows)
        {
            return resultRows[0].PartitionKey;
        }


        private bool AzureResultRowsAreProperlyFormatted(List<AzureAuthorizationKey> resultRows)
        {
            return resultRows.Count == 1
                   && !string.IsNullOrEmpty(resultRows[0].PartitionKey)
                   && !string.IsNullOrEmpty(resultRows[0].RowKey);
        }

        private List<AzureAuthorizationKey> GetAzureTableAuthorizationQueryResult(EmprintWebOrganizationsRequest request)
        {
            return _authorizationKeyTable.ExecuteQuerySegmentedAsync(GetAuthorizationQuery(request),
                new TableContinuationToken()).GetAwaiter().GetResult().Results;
        }
        private List<AzureAuthorizationKey> GetAzureTableAuthorizationQueryResult(EmprintWebFacilitiesRequest request)
        {
            return _authorizationKeyTable.ExecuteQuerySegmentedAsync(GetAuthorizationQuery(request),
                new TableContinuationToken()).GetAwaiter().GetResult().Results;
        }
        private List<AzureAuthorizationKey> GetAzureTableAuthorizationQueryResult(EmprintWebUserRequest request)
        {
            return _authorizationKeyTable.ExecuteQuerySegmentedAsync(GetAuthorizationQuery(request),
                new TableContinuationToken()).GetAwaiter().GetResult().Results;
        }

        private bool IsProperlyFormattedSecurityToken(EmprintWebOrganizationsRequest request)
        {
            return !string.IsNullOrEmpty(request.RequesterInformation.SecurityToken);
        }

        private bool IsProperlyFormattedSecurityToken(EmprintWebFacilitiesRequest request)
        {
            return !string.IsNullOrEmpty(request.RequesterInformation.SecurityToken);
        }

        private bool IsProperlyFormattedSecurityToken(EmprintWebUserRequest request)
        {
            return !string.IsNullOrEmpty(request.RequesterInformation.SecurityToken);
        }

        private TableQuery<AzureAuthorizationKey> GetAuthorizationQuery(EmprintWebOrganizationsRequest request)
        {
            return new TableQuery<AzureAuthorizationKey>().Where(TableQuery.GenerateFilterCondition("RowKey",
                QueryComparisons.Equal, request.RequesterInformation.SecurityToken));
        }

        private TableQuery<AzureAuthorizationKey> GetAuthorizationQuery(EmprintWebFacilitiesRequest request)
        {
            return new TableQuery<AzureAuthorizationKey>().Where(TableQuery.GenerateFilterCondition("RowKey",
                QueryComparisons.Equal, request.RequesterInformation.SecurityToken));
        }

        private TableQuery<AzureAuthorizationKey> GetAuthorizationQuery(EmprintWebUserRequest request)
        {
            return new TableQuery<AzureAuthorizationKey>().Where(TableQuery.GenerateFilterCondition("RowKey",
                QueryComparisons.Equal, request.RequesterInformation.SecurityToken));
        }

    }

    public class AzureAuthorizationKey : TableEntity {}
}
