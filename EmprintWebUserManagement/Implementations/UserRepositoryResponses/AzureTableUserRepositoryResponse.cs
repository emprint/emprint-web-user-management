﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Implementations.UserRepositoryResponses
{
    class AzureTableUserRepositoryResponse : IUserRepositoryResponse
    {
        public ErrorCode Error { get; set; }
        public string ErrorMessage { get; set; }
        public List<EmprintWebUserFacilityPermissions> FacilityPermissions { get; set; }
        public EmprintWebUserOrganizationPermissions OrganizationPermissions { get; set; }
        public string Username { get; set; }
    }
}
