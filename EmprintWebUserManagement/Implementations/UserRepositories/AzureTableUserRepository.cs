﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Implementations.AuthorizerResponses;
using EmprintWebUserManagement.Implementations.UserRepositoryResponses;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Security.Claims;

namespace EmprintWebUserManagement.Implementations.UserRepositories
{
    public class AzureTableUserRepository : IUserRepository
    {
        private readonly CloudTable _userRepositoryTable;
        private AzureTableUserRepositoryResponse _response;
        private string _domain;
        readonly Regex _disallowedCharsInRowKey = new Regex(@"[\\\\#%+/?\u0000-\u001F\u007F-\u009F]");

        private string _emageConnectionString =
                "Server=tcp:k4bmduivpe.database.windows.net,1433;Initial Catalog=emage_dev;Persist Security Info=False;User ID=emprint_admin;Password=EmPr1nt14;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
            ;

        public AzureTableUserRepository(string connectionString, string tableName)
        {
            
            _userRepositoryTable = CloudStorageAccount.Parse(connectionString)
                .CreateCloudTableClient()
                .GetTableReference(tableName);
            _response = new AzureTableUserRepositoryResponse
            {
                FacilityPermissions = new List<EmprintWebUserFacilityPermissions>(),
                OrganizationPermissions = new EmprintWebUserOrganizationPermissions(),
                Error = ErrorCode.None
            };
        }

        public void Create(EmprintWebUserRequest request)
        {
            //A bit ugly, but it gets the job done
            if (request.OrganizationPermissions?.ApplyPhi != null &&
                request.OrganizationPermissions?.OrganizationId != null)
            {
                var permissionEntity = ReadUserPermissionsEntity(request.Username);
                if (permissionEntity == null)
                {
                    AddNewOrganizationPermission(CreateUserAccountDto(new EmprintWebUserFacilityPermissions
                    {
                        ApplyPhi = false,
                        Facility = "0",
                        OrganizationId = "0"
                    }, request.Username), request.OrganizationPermissions);
                }
                else
                {
                    UpdateExistingOrganizationPermission(CreateUserAccountDto(new EmprintWebUserFacilityPermissions
                    {
                        ApplyPhi = false,
                        Facility = "0",
                        OrganizationId = "0"
                    }, request.Username), request.OrganizationPermissions, permissionEntity);
                }
            }

            foreach (var permission in request.FacilityPermissions)
            {
                var userAccountDto = CreateUserAccountDto(permission, request.Username);
                var permissionsEntity = ReadUserPermissionsEntity(request.Username);
                if (permissionsEntity == null)
                {
                    AddNewPermission(userAccountDto);
                }
                else
                {
                    UpdateExistingFacilityPermission(permissionsEntity, userAccountDto);
                }
            }
        }

        private void UpdateExistingFacilityPermission(Permission existingPermissionsEntity, UserAccountDto userAccountDto)
        {
            var existingUserPermissions =
                JsonConvert.DeserializeObject<UserPermissions>(existingPermissionsEntity.UserPermissionsJSON);
            var existingUserFacilityPermissions =
                existingUserPermissions.UserFacilityPermissions.SingleOrDefault(
                    p => p.FacilityId == userAccountDto.FacilityId);
            if (existingUserFacilityPermissions == null)
            {
                UpdateWithNewFacilityPermissions(existingUserPermissions, userAccountDto);
            }
            else
            {
                UpdateExistingFacilityPermissions(existingUserFacilityPermissions, userAccountDto);
            }

            existingPermissionsEntity.UserPermissionsJSON = JsonConvert.SerializeObject(existingUserPermissions);
            existingPermissionsEntity.PermissionsLastModifiedDate = DateTime.UtcNow;
            existingPermissionsEntity.IsActive = userAccountDto.IsActive;
            existingPermissionsEntity.EmServerFormFavorites = userAccountDto.EmServerFormFavorites;
            existingPermissionsEntity.EmServerPacketFavorites = userAccountDto.EmServerPacketFavorites;

            var insertOp = TableOperation.InsertOrReplace(existingPermissionsEntity);
            _userRepositoryTable.ExecuteAsync(insertOp).GetAwaiter().GetResult();
        }

        private void UpdateExistingFacilityPermissions(UserFacilityPermissions facilityPermissions, UserAccountDto userAccountDto)
        {
            if (userAccountDto.CanViewPHI)
            {
                facilityPermissions.Add(FacilityPermission.ApplyPHI);
            }
            else
            {
                facilityPermissions.Permissions.Remove(FacilityPermission.ApplyPHI);
            }

            //This is the sticky facility (facility > org)
            if (!userAccountDto.OrganizationId.Equals(facilityPermissions.OrganizationId))
            {
                facilityPermissions.OrganizationId = userAccountDto.OrganizationId;
            }
        }

        private void UpdateWithNewFacilityPermissions(UserPermissions userPermissions, UserAccountDto userAccountDto)
        {
            var newUserFacilityPermissions = new UserFacilityPermissions(userAccountDto.FacilityId, userAccountDto.OrganizationId);
            if (userAccountDto.CanViewPHI)
            {
                newUserFacilityPermissions.Add(FacilityPermission.ApplyPHI);
            }
            userPermissions.UserFacilityPermissions.Add(newUserFacilityPermissions);
        }

        private void UpdateExistingOrganizationPermission(UserAccountDto userAccountDto,
            EmprintWebUserOrganizationPermissions organizationPermissions, Permission permission)
        {
            var existingUserPermissions =
                JsonConvert.DeserializeObject<UserPermissions>(permission.UserPermissionsJSON);
            if (existingUserPermissions.UserOrganizationPermissions != null)
            {
                existingUserPermissions.UserOrganizationPermissions.OrganizationId =
                    int.Parse(organizationPermissions.OrganizationId);
            }
            else
            {
                existingUserPermissions.UserOrganizationPermissions =
                    new UserOrganizationPermissions(int.Parse(organizationPermissions.OrganizationId));

            }

            if (organizationPermissions.ApplyPhi)
            {
                existingUserPermissions.UserOrganizationPermissions.Permissions.Add(FacilityPermission.ApplyPHI);
            }
            else
            {
                existingUserPermissions.UserOrganizationPermissions.Permissions.Remove(FacilityPermission.ApplyPHI);
            }


            permission.UserPermissionsJSON = JsonConvert.SerializeObject(existingUserPermissions);
            permission.PermissionsLastModifiedDate = DateTime.UtcNow;
            permission.IsActive = userAccountDto.IsActive;
            permission.EmServerFormFavorites = userAccountDto.EmServerFormFavorites;
            permission.EmServerPacketFavorites = userAccountDto.EmServerPacketFavorites;

            var insertOp = TableOperation.InsertOrReplace(permission);
            _userRepositoryTable.ExecuteAsync(insertOp).GetAwaiter().GetResult();
        }

        private void AddNewOrganizationPermission(UserAccountDto userAccountDto, EmprintWebUserOrganizationPermissions organizationPermissions)
        {
            if (!IsUsernameValidForRowKey(userAccountDto)) return;
            var newUserPermissions = new UserPermissions(userAccountDto.Username, _domain)
            {
                UserOrganizationPermissions = new UserOrganizationPermissions(int.Parse(organizationPermissions.OrganizationId))
            };

            if (organizationPermissions.ApplyPhi)
            {
                newUserPermissions.UserOrganizationPermissions.Permissions.Add(FacilityPermission.ApplyPHI);
            }
            else
            {
                newUserPermissions.UserOrganizationPermissions.Permissions.Remove(FacilityPermission.ApplyPHI);
            }


            var permissionEntity = new Permission
            {
                PartitionKey = _domain,
                RowKey = userAccountDto.Username,
                EmageFacilityId = userAccountDto.FacilityId,
                EmServerCreatedDate = userAccountDto.EmServerCreatedDate,
                EmServerLastModifiedDate = userAccountDto.EmServerModifiedDate,
                IsActive = userAccountDto.IsActive,
                UserPermissionsJSON = JsonConvert.SerializeObject(newUserPermissions),
                PermissionsLastModifiedDate = DateTime.UtcNow,
                EmServerUserId = userAccountDto.EmServerUserId,
                EmServerFormFavorites = userAccountDto.EmServerFormFavorites,
                EmServerPacketFavorites = userAccountDto.EmServerPacketFavorites
            };
            var insertOp = TableOperation.InsertOrReplace(permissionEntity);
            _userRepositoryTable.ExecuteAsync(insertOp).GetAwaiter().GetResult();
        }
        private void AddNewPermission(UserAccountDto userAccountDto)
        {
            if (!IsUsernameValidForRowKey(userAccountDto)) return;
            var newUserPermissions = CreateNewUserPermissions(userAccountDto);
            var permissionEntity = new Permission
            {
                PartitionKey = _domain,
                RowKey = userAccountDto.Username,
                EmageFacilityId = userAccountDto.FacilityId,
                EmServerCreatedDate = userAccountDto.EmServerCreatedDate,
                EmServerLastModifiedDate = userAccountDto.EmServerModifiedDate,
                IsActive = userAccountDto.IsActive,
                UserPermissionsJSON = JsonConvert.SerializeObject(newUserPermissions),
                PermissionsLastModifiedDate = DateTime.UtcNow,
                EmServerUserId = userAccountDto.EmServerUserId,
                EmServerFormFavorites = userAccountDto.EmServerFormFavorites,
                EmServerPacketFavorites = userAccountDto.EmServerPacketFavorites
            };
            var insertOp = TableOperation.InsertOrReplace(permissionEntity);
            _userRepositoryTable.ExecuteAsync(insertOp).GetAwaiter().GetResult();
        }

        private UserPermissions CreateNewUserPermissions(UserAccountDto userAccountDto)
        {
            var userPermissions = new UserPermissions(userAccountDto.Username, _domain);
            var userFacilityPermissions = new UserFacilityPermissions(userAccountDto.FacilityId, userAccountDto.OrganizationId);
            if (userAccountDto.CanViewPHI)
                userFacilityPermissions.Add(FacilityPermission.ApplyPHI);
            userPermissions.UserFacilityPermissions.Add(userFacilityPermissions);
            return userPermissions;
        }

        public class UserPermissions
        {
            public string Username { get; }
            public string Domain { get; }

            public readonly List<UserFacilityPermissions> UserFacilityPermissions = new List<UserFacilityPermissions>();
            public UserOrganizationPermissions UserOrganizationPermissions;

            public UserPermissions(string username, string domain)
            {
                if (string.IsNullOrWhiteSpace(username))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
                if (string.IsNullOrWhiteSpace(domain))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(domain));

                Username = username;
                Domain = domain;
            }
        }
        public class UserOrganizationPermissions
        {
            public HashSet<FacilityPermission> Permissions;
            public int OrganizationId { get; set; }

            private UserOrganizationPermissions(int organizationId, string claimString)
            {
                OrganizationId = organizationId;
                var encoding = Encoding.ASCII.GetBytes(claimString);
                Permissions = new HashSet<FacilityPermission>(encoding.Select(i => (FacilityPermission)i));
            }

            public UserOrganizationPermissions(int organizationId)
            {
                OrganizationId = organizationId;
                Permissions = new HashSet<FacilityPermission>();
            }

            public static UserOrganizationPermissions FromClaim(int organizationId, Claim claim)
            {
                return new UserOrganizationPermissions(organizationId, claim.Value);
            }

            public string ToClaimString()
            {
                return Encoding.ASCII.GetString(ToByteArray());
            }
            private byte[] ToByteArray()
            {
                return Permissions.Select(p => (byte)p).ToArray();
            }

            public void Add(FacilityPermission permission)
            {
                Permissions.Add(permission);
            }

            public bool Has(FacilityPermission permission)
            {
                return Permissions.Contains(permission);
            }
        }

        public class UserFacilityPermissions
        {
            public HashSet<FacilityPermission> Permissions;
            public int FacilityId { get; private set; }
            public int OrganizationId { get; set; }

            private UserFacilityPermissions(int facilityId, int organizationId, string claimsString)
            {
                FacilityId = facilityId;
                OrganizationId = organizationId;
                var encoding = Encoding.ASCII.GetBytes(claimsString);
                Permissions = new HashSet<FacilityPermission>(encoding.Select(i => (FacilityPermission)i));
            }

            public UserFacilityPermissions(int facilityId, int organizationId)
            {
                FacilityId = facilityId;
                OrganizationId = organizationId;
                Permissions = new HashSet<FacilityPermission>();
            }
            public static UserFacilityPermissions FromClaim(int facilityId, int organizationId, Claim claim)
            {
                return new UserFacilityPermissions(facilityId, organizationId, claim.Value);
            }

            public string ToClaimsString()
            {
                return Encoding.ASCII.GetString(ToByteArray());
            }
            private byte[] ToByteArray()
            {
                return Permissions.Select(p => (byte)p).ToArray();
            }

            public void Add(FacilityPermission permission)
            {
                Permissions.Add(permission);
            }

            public bool Has(FacilityPermission permission)
            {
                return Permissions.Contains(permission);
            }
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum FacilityPermission : byte
        {
            ViewEforms = 0,
            ViewPackets = 1,
            ViewApprovedForms = 2,
            ViewApprovedPackets = 3,
            ViewStandardForms = 4,
            ViewReports = 5,
            ViewZynx = 6,
            ViewProofs = 7,
            ViewCoders = 8,
            ApproveProofs = 9,
            ApplyPHI = 10,
            EditPackets = 11,
            CreatePackets = 12,
            AdministerUsers = 13,
            ViewResources = 14,
            ViewCorporateProofs = 15,
            ViewDowntime = 16
        }

        private bool IsUsernameValidForRowKey(UserAccountDto userAccount)
        {
            return !_disallowedCharsInRowKey.IsMatch(userAccount.Username);
        }

        private Permission ReadUserPermissionsEntity(string userName)
        {
            var readOperation = TableOperation.Retrieve<Permission>(_domain, userName);
            return _userRepositoryTable.ExecuteAsync(readOperation).GetAwaiter().GetResult().Result as Permission;
        }

        public class Permission : TableEntity
        {
            public string UserPermissionsJSON { get; set; } = String.Empty;
            public DateTime? PermissionsCreatedDate { get; set; }
            public DateTime? PermissionsLastModifiedDate { get; set; }
            public DateTime? EmServerCreatedDate { get; set; }
            public DateTime? EmServerLastModifiedDate { get; set; }
            public int EmServerUserId { get; set; }
            public bool IsActive { get; set; }
            public string EmServerFormFavorites { get; set; }
            public string EmServerPacketFavorites { get; set; }
            public int EmageFacilityId { get; set; }
        }

        private UserAccountDto CreateUserAccountDto(EmprintWebUserFacilityPermissions permissions, string userName)
        {
            return new UserAccountDto
            {
                Username = userName,
                Firstname = "",
                Lastname = "",
                CanViewPHI = permissions.ApplyPhi,
                EmServerCreatedDate = null,
                EmServerFormFavorites = null,
                EmServerModifiedDate = null,
                EmServerPacketFavorites = null,
                EmServerUserId = 0,
                FacilityId = int.Parse(permissions.Facility),
                OrganizationId = int.Parse(permissions.OrganizationId),
                IsActive = true
            };
        }

        private class UserAccountDto
        {
            public int EmServerUserId { get; set; }
            public string Username { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public bool CanViewPHI { get; set; }
            public bool IsActive { get; set; }
            public DateTime? EmServerCreatedDate { get; set; }
            public DateTime? EmServerModifiedDate { get; set; }
            public string EmServerFormFavorites { get; set; }
            public string EmServerPacketFavorites { get; set; }
            public int FacilityId { get; set; }
            public int OrganizationId { get; set; }

        }

        public void Read(EmprintWebUserRequest request)
        {
            var permissions = ReadUserPermissionsEntity(request.Username);
            if (permissions != null)
            {
                var permissionsGroup = JsonConvert
                    .DeserializeObject<UserPermissions>(permissions.UserPermissionsJSON);
                foreach (var facilityPermission in permissionsGroup.UserFacilityPermissions)
                {
                    _response.FacilityPermissions.Add(new EmprintWebUserFacilityPermissions
                    {
                        Facility = facilityPermission.FacilityId.ToString(),
                        ApplyPhi = facilityPermission.Permissions.Contains(FacilityPermission.ApplyPHI),
                        OrganizationId = facilityPermission.OrganizationId.ToString()
                        
                    });
                }
                if (permissionsGroup.UserOrganizationPermissions != null)
                {
                    _response.OrganizationPermissions = new EmprintWebUserOrganizationPermissions
                    {
                        ApplyPhi = permissionsGroup.UserOrganizationPermissions.Permissions.Contains(FacilityPermission
                                       .ApplyPHI),
                        OrganizationId = permissionsGroup.UserOrganizationPermissions.OrganizationId.ToString()
                    };
                }
            }
            else
            {
                _response.Error = ErrorCode.NotFound;
                _response.ErrorMessage = $"No resource '{request.Username}' found.";
            }
        }

        public void Update(EmprintWebUserRequest request)
        {
            var permissions = ReadUserPermissionsEntity(request.Username);
            if (permissions != null)
            {
                var deleteOperation = TableOperation.Delete(permissions);
                _userRepositoryTable.ExecuteAsync(deleteOperation).GetAwaiter().GetResult();
            }

            if (request.OrganizationPermissions?.ApplyPhi != null &&
                request.OrganizationPermissions?.OrganizationId != null)
            {
                var permissionEntity = ReadUserPermissionsEntity(request.Username);
                if (permissionEntity == null)
                {
                    AddNewOrganizationPermission(CreateUserAccountDto(new EmprintWebUserFacilityPermissions
                    {
                        ApplyPhi = false,
                        Facility = "0",
                        OrganizationId = "0"
                    }, request.Username), request.OrganizationPermissions);
                }
                else
                {
                    UpdateExistingOrganizationPermission(CreateUserAccountDto(new EmprintWebUserFacilityPermissions
                    {
                        ApplyPhi = false,
                        Facility = "0",
                        OrganizationId = "0"
                    }, request.Username), request.OrganizationPermissions, permissionEntity);
                }
            }

            foreach (var permission in request.FacilityPermissions)
            {
                var userAccountDto = CreateUserAccountDto(permission, request.Username);
                var permissionsEntity = ReadUserPermissionsEntity(request.Username);
                if (permissionsEntity == null)
                {
                    AddNewPermission(userAccountDto);
                }
                else
                {
                    UpdateExistingFacilityPermission(permissionsEntity, userAccountDto);
                }
            }
            //permissions.Or
        }

        public void Delete(EmprintWebUserRequest request)
        {
            var permissions = ReadUserPermissionsEntity(request.Username);
            if (permissions != null)
            {
                var deleteOperation = TableOperation.Delete(permissions);
                _userRepositoryTable.ExecuteAsync(deleteOperation).GetAwaiter().GetResult();
            }
            else
            {
                _response.Error = ErrorCode.NotFound;
                _response.ErrorMessage = $"No resource '{request.Username}' found to delete.";
            }
        }

        public void HandleUnknownOperation(EmprintWebUserRequest request)
        {
            _response.Error = ErrorCode.Unknown;
            _response.ErrorMessage = "Unknown Operation";
        }

        public IUserRepositoryResponse HandleUserRequest(EmprintWebUserRequest request, IAuthorizerResponse authorizerResponse)
        {
            _domain = ((AzureTableAuthorizerResponse) authorizerResponse).Domain;
            if (!String.IsNullOrEmpty(request.Username))
                _response.Username = request.Username;

            switch (request.Operation)
            {
                case Operation.Create:
                    Create(request);
                    break;
                case Operation.Read:
                    Read(request);
                    break;
                case Operation.Update:
                    Update(request);
                    break;
                case Operation.Delete:
                    Delete(request);
                    break;
                default:
                    HandleUnknownOperation(request);
                    break;
            }

            return _response;
        }
    }
}
