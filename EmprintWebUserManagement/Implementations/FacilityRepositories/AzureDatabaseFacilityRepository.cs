﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Implementations.AuthorizerResponses;
using EmprintWebUserManagement.Implementations.FacilityRepositoryResponses;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;
using System.Data.SqlClient;
using System.Linq;

namespace EmprintWebUserManagement.Implementations.FacilityRepositories
{
    public class AzureDatabaseFacilityRepository : IFacilityRepository
    {
        private string _domain;
        private AzureDatabaseFacilityRepositoryResponse _response;
        private string _emageConnectionString =
                "Server=tcp:k4bmduivpe.database.windows.net,1433;Initial Catalog=emage_dev;Persist Security Info=False;User ID=emprint_admin;Password=EmPr1nt14;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
            ;
        public AzureDatabaseFacilityRepository()
        {

            _response = new AzureDatabaseFacilityRepositoryResponse
            {
                Error = ErrorCode.None
            };
        }


        public IFacilityRepositoryResponse HandleFacilitiesRequest(EmprintWebFacilitiesRequest request,
            IAuthorizerResponse authorizerResponse)
        {
            _domain = ((AzureTableAuthorizerResponse) authorizerResponse).Domain;

            switch (request.Operation)
            {
                case Operation.Read:
                    Read(request);
                    break;
                default:
                    HandleUnknownOperation(request);
                    break;
            }

            return _response;
        }

        public void HandleUnknownOperation(EmprintWebFacilitiesRequest request)
        {
            _response.Error = ErrorCode.Unknown;
            _response.ErrorMessage = "Unknown Operation";
        }

        public void Read(EmprintWebFacilitiesRequest request)
        {
            _response.Facilities = GetFacilities(request);
        }

        private List<EmprintWebFacility> GetFacilities(EmprintWebFacilitiesRequest request)
        {
            var results = new List<EmprintWebFacility>();
            var query = GetFacilityQuery(request);
            using (var sqlConnection = new SqlConnection(_emageConnectionString))
            {
                sqlConnection.Open();
                var reader = new SqlCommand(query, sqlConnection).ExecuteReader();
                while (reader.Read())
                {
                    results.Add(new EmprintWebFacility
                    {
                        Id = $"{reader.GetInt32(0)}",
                        Name = $"{reader.GetString(3).PadLeft(4, '0')} {reader.GetString(1)}",
                        OrganizationId = $"{reader.GetInt32(2)}"
                    });
                }
            }
            return results;
        }

        private string GetFacilityQuery(EmprintWebFacilitiesRequest request)
        {
            return
                $"SELECT F.Id, F.Name, F.OrganizationId, F.FacilityNumber FROM Facilities F JOIN Organizations O ON O.Id = F.OrganizationId AND O.PortalSubdomain = '{_domain}'";
        }
    }
}
