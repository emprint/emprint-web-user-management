﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.Interfaces;

namespace EmprintWebUserManagement.Implementations.AuthorizerResponses
{
    public class AzureTableAuthorizerResponse : IAuthorizerResponse
    {
        public bool IsAuthorized { get; set; }
        public string Domain { get; set; }
    }
}
