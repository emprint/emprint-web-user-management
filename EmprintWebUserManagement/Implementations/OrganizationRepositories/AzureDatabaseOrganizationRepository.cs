﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using EmprintWebUserManagement.DTO;
using EmprintWebUserManagement.Implementations.AuthorizerResponses;
using EmprintWebUserManagement.Implementations.FacilityRepositoryResponses;
using EmprintWebUserManagement.Implementations.OrganizationRepositoryResponses;
using EmprintWebUserManagement.Interfaces;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.Implementations.OrganizationRepositories
{
   public class AzureDatabaseOrganizationRepository : IOrganizationRepository
    {
        private string _domain;
        private AzureDatabaseOrganizationRepositoryResponse _response;
        private string _emageConnectionString =
                "Server=tcp:k4bmduivpe.database.windows.net,1433;Initial Catalog=emage_dev;Persist Security Info=False;User ID=emprint_admin;Password=EmPr1nt14;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
            ;
        public AzureDatabaseOrganizationRepository()
        {

            _response = new AzureDatabaseOrganizationRepositoryResponse
            {
                Error = ErrorCode.None
            };
        }


        public IOrganizationRepositoryResponse HandleOrganizationsRequest(EmprintWebOrganizationsRequest request,
            IAuthorizerResponse authorizerResponse)
        {
            _domain = ((AzureTableAuthorizerResponse)authorizerResponse).Domain;

            switch (request.Operation)
            {
                case Operation.Read:
                    Read(request);
                    break;
                default:
                    HandleUnknownOperation(request);
                    break;
            }

            return _response;
        }

        public void HandleUnknownOperation(EmprintWebOrganizationsRequest request)
        {
            _response.Error = ErrorCode.Unknown;
            _response.ErrorMessage = "Unknown Operation";
        }

        public void Read(EmprintWebOrganizationsRequest request)
        {
            _response.Organizations = GetOrganizations(request);
        }

        private List<EmprintWebOrganization> GetOrganizations(EmprintWebOrganizationsRequest request)
        {
            var results = new List<EmprintWebOrganization>();
            var query = GetFacilityQuery(request);
            using (var sqlConnection = new SqlConnection(_emageConnectionString))
            {
                sqlConnection.Open();
                var reader = new SqlCommand(query, sqlConnection).ExecuteReader();
                while (reader.Read())
                {
                    results.Add(new EmprintWebOrganization
                    {
                        Id = $"{reader.GetInt32(0)}",
                        Name = reader.GetString(1),
                    });
                }
            }
            return results;
        }

        private string GetFacilityQuery(EmprintWebOrganizationsRequest request)
        {
            return
                $"SELECT O.Id, O.Name FROM Organizations O WHERE O.PortalSubdomain = '{_domain}'";
        }
    }
}
