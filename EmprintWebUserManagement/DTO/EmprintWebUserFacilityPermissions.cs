﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebUserFacilityPermissions
    {
        public bool ApplyPhi { get; set; }
        public string Facility { get; set; }
        public string OrganizationId { get; set; }
    }
}
