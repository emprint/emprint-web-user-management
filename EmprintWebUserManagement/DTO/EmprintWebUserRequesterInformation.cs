﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebUserRequesterInformation
    {
        public string SecurityToken { get; set; }
    }
}
