﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebFacilitiesRequest
    {
        public EmprintWebFacilitiesRequesterInformation RequesterInformation { get; set; }
        public Operation Operation { get; set; }
    }
}
