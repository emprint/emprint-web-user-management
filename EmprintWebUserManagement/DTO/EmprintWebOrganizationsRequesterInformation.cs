﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebOrganizationsRequesterInformation
    {
        public string SecurityToken { get; set; }
    }
}
