﻿using System.Collections.Generic;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebUserRequestResponse
    {
        public ErrorCode Error { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsAuthorized { get; set; }
        public List<EmprintWebUserFacilityPermissions> FacilityPermissions { get; set; }
        public EmprintWebUserOrganizationPermissions OrganizationPermissions { get; set; }
        public string Username { get; set; }
    }
}
