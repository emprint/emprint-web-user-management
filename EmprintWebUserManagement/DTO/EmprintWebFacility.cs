﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebFacility
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string OrganizationId { get; set; }
    }
}
