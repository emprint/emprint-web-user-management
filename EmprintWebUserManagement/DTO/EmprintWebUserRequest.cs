﻿using System.Collections.Generic;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebUserRequest
    {
        public string Username { get; set; }
        public EmprintWebUserRequesterInformation RequesterInformation { get; set; }
        public List<EmprintWebUserFacilityPermissions> FacilityPermissions { get; set; }
        public EmprintWebUserOrganizationPermissions OrganizationPermissions { get; set; }
        public Operation Operation { get; set; }
    }
}
