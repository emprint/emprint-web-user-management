﻿using System;
using System.Collections.Generic;
using System.Text;
using EmprintWebUserManagement.Utility;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebFacilitiesRequestResponse
    {
        public ErrorCode Error { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsAuthorized { get; set; }
        public List<EmprintWebFacility> Facilities { get; set; }
    }
}
