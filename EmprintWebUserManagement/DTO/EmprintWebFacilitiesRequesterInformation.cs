﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmprintWebUserManagement.DTO
{
    public class EmprintWebFacilitiesRequesterInformation
    {
        public string SecurityToken { get; set; }
    }
}
